package aspect;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author nshoef
 * This class demonstrate the work of a PriceHolder. It creates a number of producers which produce random prices 
 * for item a and item b as well as consumer for each item.
 * The producer print a message when it send a price to the priceHolder and the consumer print a message when it consume 
 * a price.
 */
public class ExerciseTest {

	public static void main(String[] args) {
		ExerciseTest test = new ExerciseTest();
		test.run();	
	}
	
	public void run() {
		
		PriceHolder3 ph = new PriceHolder3();
		List<String> items = Arrays.asList(new String[] {"a", "b"});
		Thread consumerA = new Thread(getConsumer(ph, "a"));
		Thread consumerB = new Thread(getConsumer(ph, "b"));
		consumerA.start();
		consumerB.start();
		for(int i=1; i<5;i++) {
			Thread producer = new Thread(getRandomPriceProducer(ph, items));
			producer.setName("p"+i);
			producer.start();	
		}
	}
	/**
	 * Creates a price consumer
	 * @param ph the price holder to consume prices from
	 * @param item the item to consume it's price
	 * @return a runnable implementation for a price consumer
	 */
	private Runnable getConsumer(PriceHolder3 ph, String item) {
		
		Runnable consumer = new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					try {
						System.out.println("a:"+ph.waitForNextPrice(item));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		return consumer;
	}
	
	/**
	 * Create a price producer runnable.
	 * @param ph the price holder to send the prices to.
	 * @param items the items to be priced
	 * @return a price producer runnable
	 */
	private Runnable getRandomPriceProducer(PriceHolder3 ph, List<String> items) {
		
		Runnable producer = new Runnable() { 
			
			@Override
			public void run() {
				while(true) {
					try {
						String item = items.get((int) (items.size()*Math.random()));
						sendPrice(item);
						Thread.sleep((long)(Math.random()*1000)); //that is to make things a bit more random
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			private synchronized void sendPrice(String name) {
				BigDecimal price = new BigDecimal(10*Math.random()).setScale(2, BigDecimal.ROUND_DOWN);
				System.out.println(Thread.currentThread().getName()+" sending price: "+name+"="+price);
				ph.putPrice(name, price);	
			}
		};	
		return producer;
	}
}
