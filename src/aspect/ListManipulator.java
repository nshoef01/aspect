package aspect;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

public class ListManipulator<T> {
	
	
	public static <T> List<T> map(Function<T, T> f, List<T> source) {
		List<T> result = new ArrayList<>();
		source.forEach(x -> result.add(f.apply(x)));
		return result;
	}

	

	public static void main(String[] args) {
		Function<Integer, Integer> f = (x) -> x+2;
		
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(3);
		list.add(7);
		
		List<Integer> result = map(f, list);
		result.forEach(x -> System.out.println(x));
		list.forEach(x -> System.out.println(x));
		
	}

}
