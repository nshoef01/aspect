package aspect;
import java.math.BigDecimal;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

public final class PriceHolder3 {
	private final ConcurrentMap<String, BigDecimal> priceMap;
	private final Set<String> changed;
	private final Object lock;
	
	public PriceHolder3() {
		priceMap = new ConcurrentHashMap<>();
		changed = new CopyOnWriteArraySet<>();
		lock = new Object();
		
		
	}
	/** Called when a price �p� is received for an entity �e� */

	public void putPrice(String e, BigDecimal p) {
		synchronized(lock) {
			priceMap.put(e, p);
			changed.add(e);
			lock.notifyAll();
		}	
	}

	/** Called to get the latest price for entity �e� */

	public BigDecimal getPrice(String e) {
		synchronized(lock) {
			 changed.remove(e);
			 return priceMap.get(e);		
		}	
	}

	/**

	* Called to determine if the price for entity �e� has

	* changed since the last call to getPrice(e).

	*/

	public boolean hasPriceChanged(String e) {
		return changed.contains(e);
	}

	public BigDecimal waitForNextPrice(String e) throws InterruptedException {
		while(!hasPriceChanged(e)) {
			synchronized(lock) {
				lock.wait();
			}		
		}
		return getPrice(e);
	}
		
}
